import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.css']
})
export class FormModalComponent implements OnInit {
  formUser: FormGroup;
  isUserAdded: boolean = false;
  constructor(public activeModal: NgbActiveModal,
              private fb: FormBuilder,private http: HttpClient) { }

  ngOnInit() {
  	this.formUser = this.fb.group({
       	nom: new FormControl('',[
       			Validators.required
       		]),
       	dateNaissance: new FormControl('',[
       			Validators.required
       		]),
       	adress: new FormControl('',[
       			Validators.required
       		]),
       	siteWeb: new FormControl('',[
       			Validators.required
       		]),
       	email: new FormControl('',[
       			Validators.required,
       			Validators.email
       		]),
    });
  }

  closeModal() {
  	this.activeModal.close('Modal Closed');
  }
  get nom() { return this.formUser.get('nom'); }
  get dateNaissance() { return this.formUser.get('dateNaissance'); }
  get adress() { return this.formUser.get('adress'); }
  get siteWeb() { return this.formUser.get('siteWeb'); }
  get email() { return this.formUser.get('email'); }

  register(form) {
    let postData = new FormData();
    postData.append('nom' , this.formUser.get('adress').value);
    postData.append('dateNaissance' , this.formUser.get('dateNaissance').value);
    postData.append('siteWeb' , this.formUser.get('siteWeb').value);
    postData.append('email' , this.formUser.get('email').value);
    postData.append('adress' , this.formUser.get('adress').value);
   this.http.post("http://127.0.0.1:8090/test-technique-back-end-php/database/register.php", postData)
    .subscribe(data => {
        this.isUserAdded = true;
        this.formUser.reset;
    });
  }
}
