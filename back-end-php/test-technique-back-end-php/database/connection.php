<?php

function getConnection(){
	$connectionChaine = "mysql:host=localhost;dbname=test-tech-db;charset=utf8";
	$user = "admin";
	$password = "admin";
	$connection = new PDO($connectionChaine,$user,$password);
	if(!$connection){
		throw new Exception("Connection faild");
	}
	return $connection;
}
?>