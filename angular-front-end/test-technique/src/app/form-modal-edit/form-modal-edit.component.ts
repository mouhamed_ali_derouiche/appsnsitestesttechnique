import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Utilisateur } from '../Utilisateur';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-form-modal-edit',
  templateUrl: './form-modal-edit.component.html',
  styleUrls: ['./form-modal-edit.component.css']
})
export class FormModalEditComponent implements OnInit {

  @Input() utilisateur: Utilisateur;
  formUser: FormGroup;
  isUserEdit: boolean = false;
  
  constructor(public activeModal: NgbActiveModal,
    private fb: FormBuilder,private http: HttpClient) { }

  ngOnInit() {
    this.formUser = this.fb.group({
         id: new FormControl(this.utilisateur.ID,[
             Validators.required
           ]),
         dateAjout: new FormControl(this.utilisateur.dateAjout,[
             Validators.required
           ]),
         nom: new FormControl(this.utilisateur.nom,[
             Validators.required
           ]),
         dateNaissance: new FormControl(this.utilisateur.dateNaissance,[
             Validators.required
           ]),
         adress: new FormControl(this.utilisateur.adress,[
             Validators.required
           ]),
         siteWeb: new FormControl(this.utilisateur.siteWeb,[
             Validators.required
           ]),
         email: new FormControl(this.utilisateur.email,[
             Validators.required,
             Validators.email
           ]),
    });
  }

  closeModal() {
  	this.activeModal.close('Modal Closed');
  }
  get id() { return this.formUser.get('id'); }
  get dateAjout() { return this.formUser.get('dateAjout'); }
  get nom() { return this.formUser.get('nom'); }
  get dateNaissance() { return this.formUser.get('dateNaissance'); }
  get adress() { return this.formUser.get('adress'); }
  get siteWeb() { return this.formUser.get('siteWeb'); }
  get email() { return this.formUser.get('email'); }

  edit(){
    let postData = new FormData();
    postData.append('id' , this.formUser.get('id').value);
    postData.append('dateAjout' , this.formUser.get('dateAjout').value);
    postData.append('nom' , this.formUser.get('adress').value);
    postData.append('dateNaissance' , this.formUser.get('dateNaissance').value);
    postData.append('siteWeb' , this.formUser.get('siteWeb').value);
    postData.append('email' , this.formUser.get('email').value);
    postData.append('adress' , this.formUser.get('adress').value);
   this.http.post("http://127.0.0.1:8090/test-technique-back-end-php/database/edit-user.php", postData)
    .subscribe(data => {
        this.isUserEdit = true;
        console.log(data);
    });
  }
}
