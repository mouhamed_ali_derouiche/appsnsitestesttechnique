import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormModalEditComponent } from './form-modal-edit.component';

describe('FormModalEditComponent', () => {
  let component: FormModalEditComponent;
  let fixture: ComponentFixture<FormModalEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormModalEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormModalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
