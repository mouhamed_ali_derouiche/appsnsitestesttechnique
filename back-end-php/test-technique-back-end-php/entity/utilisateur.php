<?php

namespace entity;

class Utilisateur
{
	private $ID;
	private $nom;
	private $dateNaissance;
	private $adress;
	private $siteWeb;
	private $email;
	private $dateAjout;

	function __construct($nom,$dateNaissance,$adress,$siteWeb,$email)
	{
		$this->nom = $nom;
		$this->dateNaissance = $dateNaissance;
		$this->adress = $adress;
		$this->siteWeb = $siteWeb;
		$this->email = $email;
		$this->dateAjout = date("dd/mm/yyyy");
	}

	public function getDateAjout(){
		return $this->dateAjout;
	}

	public function getEmail(){
		return $this->email;
	}
	public function setEmail($email){
		$this->email = $email;
	}

	public function setSiteWeb($siteWeb){
		$this->siteWeb = $siteWeb;
	}

	public function getNom(){
		return $this->nom;
	}
	public function setNom($nom){
		$this->nom = $nom;
	}

	public function getDateNaissance(){
		return $this->dateNaissance;
	}
	public function setDateNaissance($dateNaissance){
		$this->dateNaissance = $dateNaissance;
	}

	public function getAdress(){
		return $this->adress;
	}
	public function setAdress($adress){
		$this->adress = $adress;
	}

	public function getSiteWeb(){
		return $this->siteWeb;
	}
	public function setSiteWeb($siteWeb){
		$this->siteWeb = $siteWeb;
	}
}