
export class Utilisateur {
	ID: number;
	nom: string;
	dateNaissance: string;
	adress: string;
	siteWeb: string;
	email: string;
	dateAjout: string;

	constructor(id: number,
	nom: string,
	dateNaissance: string,
	adress: string,
	siteWeb: string,
	email: string,
	dataAjout: string,) {
		this.ID = id;
		this.nom = nom;
		this.dateNaissance = dateNaissance;
		this.siteWeb = siteWeb;
		this.email = email;
		this.dateAjout = dataAjout;
	}
}