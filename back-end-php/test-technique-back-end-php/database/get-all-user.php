<?php
header("Access-Control-Allow-Origin: *");
include './connection.php';

try{
	$connection = getConnection();
	$res = $connection->query('SELECT * FROM `UTILISATEUR`');
	$listUserJson = json_encode($res->fetchAll(PDO::FETCH_ASSOC));
	echo $listUserJson;
}catch(Exception $ex){
	echo $ex->getMessage();
}