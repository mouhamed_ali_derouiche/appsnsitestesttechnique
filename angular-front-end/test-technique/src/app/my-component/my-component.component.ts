import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormModalComponent } from '../form-modal/form-modal.component';
import { FormModalEditComponent } from '../form-modal-edit/form-modal-edit.component';
import { Utilisateur } from '../Utilisateur';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponentComponent implements OnInit {
  
  listAllUser: any;
  isUserDeleted: boolean = false;
  constructor(private http: HttpClient, private modalService: NgbModal,
              private location: Location) { }

  ngOnInit() {

  	this.http.get<any>('http://127.0.0.1:8090/test-technique-back-end-php/database/get-all-user.php').subscribe(data => {
            this.listAllUser = data;
        });
  }

openFormModal() {
  const modalRef = this.modalService.open(FormModalComponent);
  
  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });
}

openFormModalEdit(user: Utilisateur) {
  const modalRef = this.modalService.open(FormModalEditComponent);
  modalRef.componentInstance.utilisateur = user;
  modalRef.result.then((result) => {
    console.log(result);
  }).catch((error) => {
    console.log(error);
  });
}
ascUser(){
  this.http.get<any>('http://127.0.0.1:8090/test-technique-back-end-php/database/list-user-asc.php').subscribe(data => {
      this.listAllUser = data;
  });
}

descUser(){
  this.http.get<any>('http://127.0.0.1:8090/test-technique-back-end-php/database/list-user-desc.php').subscribe(data => {
      this.listAllUser = data;
  });
}

delete(id: number){
  this.http.get<any>('http://127.0.0.1:8090/test-technique-back-end-php/database/delele.php?id='+id).subscribe(data => {
      location.reload();
   });
}
}
